include <thread_profile.scad>

$fn=200;
// -> Tiny distance used as clearence between parts:
clear=0.1;
// -> Radius for roundings on edges:
rRad=2;

/* [Spindle] */
// -> main body:
mainBodyHei=196;
mainBodyDia=65;
// -> small cylinder in front:
smallCylHei=6;
smallCylDia=48.18;
// -> spindle that holds chuck:
spindleHei=12;
spindleDia=14;
// -> chuck that holds ER11 collet:
chuckHei=14.3;
chuckDia=19;

/* [611 Plate] */
// -> DXF file of 611 plate:
611PlateFile="611Plate_new.dxf";
// -> Thickness of 611 plate:
611PlateThick=4;
// -> Length of 611 plate:
611PlateLen=230;
// -> DXF file for keyhole in 611 plate:
611KeyholeFile="Keyhole.dxf";
// -> DXF file for screw holes in 611 plate:
611ScrewholeFile="Screwholes.dxf";

/* [Base Plate] */
// -> base plate that is screwed to 611 plate and holds spindle + vacuum hose:
basePlateHei=6;
basePlateWid=100;
basePlateLen=200;

/* [Spindle Holder] */
// -> 65 mm sleeve that holds the spindle
spindleHolderFile="SpindleHolder.stl";

/* [Vac Shoe] */
// -> DFX file for the base of VacShoe
vacShoeFile="VacShoe.dxf";
vacShoeHei=6;
// -> DFX file for the top of VacShoe
vacShoeTopFile="VacShoeTop32mm.dxf";
vacShoeTopHei=1.6;

/* [VacHose 32mm adapter] */
// -> Inner diameter of vacuum hose
vacHoseIDia=32;
// -> Pitch of the threads in the 32mm vacuum hose
vacHoseThreadPitch=6;
// -> How many threads should the vacuum hose adapter have?
VacHoseThreadTurns=6;
// -> Height of a tread
ThreadHei=1.32;
// -> Thread profile of the 32mm vacuum hose
function 32mmVacuumHoseProfile() = [
    [0,0],
    [0,ThreadHei],
    [ThreadHei/2,ThreadHei/2],
];

////////////////////////////////////////////////////////////////
// main
////////////////////////////////////////////////////////////////
translate([0,0,basePlateHei+611PlateThick/2])
    makeVacShoe();

union()
{
makeSpindleHolder();
makeBasePlate();
makeHolderBase();
}

color("LightBlue")
    make661Plate();

color("Lavender") 
    makeSpindle();

////////////////////////////////////////////////////////////////
module makeVacShoe()
{
    union()
    {
    translate([0,0,(vacShoeHei-vacShoeTopHei)/2])
    linear_extrude(height=vacShoeHei-vacShoeTopHei, center=true, convexity=10)
        import(file=vacShoeFile);
    translate([0,0,vacShoeHei-(vacShoeTopHei/2)-0.01])
    linear_extrude(height=vacShoeTopHei, center=true, convexity=10)
        import(file=vacShoeTopFile);
    translate([-98,0,vacShoeHei-0.05])
        makeVacHoseAdapter();
    }
}

///////////////////////////////////////////////////////////////////////////////
module makeVacHoseAdapter()
{
    difference()
    {
        translate([0,0,vacHoseThreadPitch*VacHoseThreadTurns+ThreadHei])
        mirror([0,0,1]) // mirror thread to reverse direction
        union()
        {
        straight_thread(section_profile=32mmVacuumHoseProfile(),
                       pitch=vacHoseThreadPitch,
                       turns=VacHoseThreadTurns,r=vacHoseIDia/2);
        cylinder(h=vacHoseThreadPitch*VacHoseThreadTurns+ThreadHei, d=vacHoseIDia+0.05, center=false);
        }
        
        cylinder(h=4*(vacHoseThreadPitch*VacHoseThreadTurns), d=vacHoseIDia-2*2.5, center=true);
    }
}

////////////////////////////////////////////////////////////////
module makeSpindleHolder()
{
    difference()
    {
    translate([0,0,611PlateThick/2-2])
    rotate([0,0,-90])
    import(file=spindleHolderFile, convexity=10);

    linear_extrude(height=20*611PlateThick, center=true, convexity=10)
    import(file=611KeyholeFile);
        
    linear_extrude(height=10*611PlateThick, center=true, convexity=10)
    import(file=611ScrewholeFile);

    translate([0,0,1])
    cube([basePlateLen,basePlateWid,2], center=true);
        
    translate([0,0,50+611PlateThick/2+basePlateHei])
    cylinder(h=100, d=65, center=true);
    }
}

////////////////////////////////////////////////////////////////
module makeHolderBase()
{
    translate([0,0,611PlateThick/2+basePlateHei/2])
    difference()
    {
     cylinder(h=basePlateHei, d=80, center=true);

     cylinder(h=basePlateHei+6, d=48.5, center=true);
     
     translate([0,56/2,0])
     cylinder(h=basePlateHei+6, d=4.2, center=true);

     translate([0,-56/2,0])
     cylinder(h=basePlateHei+6, d=4.2, center=true);
    }
}

////////////////////////////////////////////////////////////////
module makeBasePlate()
{
    difference()
    {
    translate([-((611PlateLen-basePlateLen)/2),0,611PlateThick/2+basePlateHei/2])
    linear_extrude(height=basePlateHei, center=true, convexity=10)
    {
        minkowski()
        {
        square([basePlateLen-2*rRad,basePlateWid-2*rRad], true);
        circle(r=rRad,$fn=200);
        }
    }
    linear_extrude(height=20*611PlateThick, center=true, convexity=10)
    import(file=611KeyholeFile);
        
    linear_extrude(height=10*611PlateThick, center=true, convexity=10)
    import(file=611ScrewholeFile);
    }
}

////////////////////////////////////////////////////////////////
module makeSpindle()
{  
  translate([0,0,611PlateThick/2])
  union()
  { 
  translate([0,0,mainBodyHei/2+smallCylHei])
    cylinder(h=mainBodyHei, d=mainBodyDia, center=true);
  translate([0,0,smallCylHei/2+clear/2])
    cylinder(h=smallCylHei+clear, d=smallCylDia, center=true);
  translate([0,0,-spindleHei/2+clear/2])
    cylinder(h=spindleHei+clear, d=spindleDia, center=true);
  translate([0,0,-spindleHei-chuckHei/2+clear/2])
    cylinder(h=chuckHei+clear, d=chuckDia, center=true, $fn=6);
  }
}

////////////////////////////////////////////////////////////////
module make661Plate()
{
linear_extrude(height=611PlateThick, center=true, convexity=10)
    import(file=611PlateFile);
}
