// creates box for ER11 collets
////////////////////////////////////////////////////////////////////

// -> bottom diameter of collet in mm
colletDiaBottom=8;
// -> top diameter of collet in mm
colletDiaTop=11.5;

// -> total height of a collet
colletHei=19;
// -> front tapered length:
// -> How deep will the collet be pushed into the base of the box? This determines the height of the lower part of the box. The upper part will be the lid.
socketHei=13.5;

// -> number of collets in the box
colletNr=8;

// -> distance between single collets in the box
colletDist=6;

///////////////////////////////////////////////////////////////////

// -> wall thickness
wallThic=1.7;
// -> the length of the box
totalLen=(colletNr*colletDiaTop) + (colletNr*colletDist) + 2*wallThic;
// -> the width of the box
totalWid=colletDist + colletDiaTop +2*wallThic;
// -> the total height of the box 
totalHei=colletHei + 4*wallThic;
// -> the height of the overlapping part in the lid
lidOve=5.0;
// -> the height of the lid
lidHei=totalHei-(socketHei + wallThic)+lidOve;
//
baseHei=totalHei-lidHei+lidOve;
// -> the radius of fillets and outer roundings
filletRad=2.0;

////////////////////////////////////////////////////////////////////

// -> increase the number for smoother edges
$fn=30;
// -> small amount that makes lid bigger than body in order to get a better fit between them on not so exact printers.
clearence=0.02;
// used for a very very very thin wall that will be extruded. 
minThic=0.05;
// size that is used for the object that cuts the containers in a plane
cutSize=max(totalLen,totalHei,totalWid);

    echo("-> main():");
    echo("    totalLen:  ", totalLen);
    echo("    totalWid:  ", totalWid);
    echo("    totalHei:  ", totalHei);
    echo("    lidHei:    ", lidHei);
    echo("    baseHei:   ", baseHei);
    echo("    socketHei: ", socketHei);
    echo("    wallThic:  ", wallThic);
    echo("    lidOve:    ", lidOve);
    echo("    filletRad: ", filletRad);
    echo("    clearence: ", clearence);
    echo("    minThic:   ", minThic);
    echo("    cutSize:   ", cutSize);

////////////////////////////////////////////////////////////////////
// main
////////////////////////////////////////////////////////////////////

pluggableContainer()
    square([totalLen, totalWid], center=true);
        
////////////////////////////////////////////////////////////////////
// modules
////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////
module pluggableContainer()
{
drillHoles(totalLen, colletDist, colletDiaBottom, colletDiaTop, socketHei)    
    makeBody(totalHei, filletRad, wallThic, lidHei, lidOve)
        children(0);

translate([0,2*totalWid,0]) 
    makeLid(totalHei, filletRad, wallThic, lidHei, lidOve)
        children(0);
}

////////////////////////////////////////////////////////////////////
module drillHoles(length, step, diaBot, diaTop, depth)
{
    difference()
    {
    children(0);
    for(x=[-length/2+step/2+diaTop/2+wallThic : step+diaTop : length/2-step/2-diaTop/2-wallThic])
        {
        //echo("-> x:  ", x);
        translate([x,0,wallThic])
        cylinder(h=depth+clearence, d1=diaBot, d2=diaTop, center=false);
        }
    }
}
////////////////////////////////////////////////////////////////////
module makeBody(height, fillet, wall, lid, overlap)
{
    union()
    {
        cutTop(height-lid)
            makeOuterBody(fillet, wall)
                children(0);

        cutTop(height-lid+overlap)
            makeSolidInnerBody(fillet, wall)
                children(0);
    }
}
////////////////////////////////////////////////////////////////////
module makeLid(height, fillet, wall, lid, overlap)
{
    union()
    {
        cutTop(lid)
            makeOuterBody(fillet, wall)
                offset(clearence)
                    children(0);

        cutTop(lid-overlap)
            makeInnerBody(fillet, wall)
                offset(clearence)
                children(0);
    }
}
////////////////////////////////////////////////////////////////////
module makeOuterBody(fillet, wall)
{
    difference()
    {
        roundedBox(fillet)
            children(0);

        translate([0,0,wall/2])
            roundedBox(fillet)
                offset(-wall/2)
                    children(0);
    }
}
////////////////////////////////////////////////////////////////////
module makeSolidInnerBody(fillet, wall)
{
    translate([0,0,wall/2])
        roundedBox(fillet)
            offset(-wall/2)
                children(0);
}
////////////////////////////////////////////////////////////////////
module makeInnerBody(fillet, wall)
{
    translate([0,0,wall/2])
    difference()
    {
        roundedBox(fillet)
            offset(-wall/2)
                children(0);

        translate([0,0,wall/2])
            roundedBox(fillet)
                offset(-wall)
                    children(0);
    }
}
//////////////////////////////////////////////////////////////////////
module roundedBox(filletRadius)
{
    hull()
    {
        translate([0,0,totalHei-filletRad])
            makeTop(filletRadius)
                offset(-filletRad)
                    children(0);

        translate([0,0,filletRad])
            makeBottom(filletRadius)
                offset(-filletRad)
                    children(0);
    }
}
////////////////////////////////////////////////////////////////////
module makeTop(filletRadius)
{    
    cutBottom(0)
        makeFillet(filletRadius)
            children(0);
}
////////////////////////////////////////////////////////////////////
module makeBottom(filletRadius)
{
    cutTop(0)
        makeFillet(filletRadius)
            children(0);
}
////////////////////////////////////////////////////////////////////
module makeFillet(filletRadius)
{
    minkowski()
    {
    sphere(r=filletRadius);
    translate([0,0,-minThic/2])
    linear_extrude(minThic, convexity=8)
        children(0);
    }
}
////////////////////////////////////////////////////////////////////
// cuts the top of a children away.
module cutTop(height2Cut)
{
    difference()
    {
        children(0);
        translate([0, 0, height2Cut+cutSize])
            cube(2*cutSize, center=true);
    }
}
////////////////////////////////////////////////////////////////////
// cuts the bottom of a children away.
module cutBottom(height2Cut)
{
    difference()
    {
        children(0);
        translate([0, 0, height2Cut-cutSize])
            cube(2*cutSize, center=true);
    }
}
//////////////////////////////////////////////////////////////////////