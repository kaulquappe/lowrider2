// -> The radius the corners will be rounded:
rdRad=0.5;


/* [Pipe] */
// -> The outside pipe diameter:
pOuterDia=25;
// -> The inside pipe diameter:
pInnerDia=21;


/* [Rim] */
// -> The height of the rim:
rHeight=2;

/* [Body] */
// -> The total height of the sleeve:
bHeight=12;
// -> The wall thickness of the sleeve:
bWall=1.6;


/* [Hidden] */
// -> Tiny distance used as clearence between parts:
clear=0.03;
// -> The inner diameter of the sleeve:
bInnerDia=pInnerDia-2*bWall;
// -> The wall thickness of the pipe:
pWall=(pOuterDia-pInnerDia)/2;

////////////////////////////////////////////////////////////////

makeRoundedInnerSleeve();

////////////////////////////////////////////////////////////////
module makeRoundedInnerSleeve()
{
rotate_extrude($fn=200)
{
    $fn=200;
    minkowski()
    {
    polygon(points=[
    [bInnerDia/2+rdRad,rdRad],
    [pInnerDia/2-rdRad-clear,rdRad],
    [pInnerDia/2-rdRad,bHeight-rHeight+rdRad],
    [pInnerDia/2+pWall-rdRad,bHeight-rHeight+rdRad],
    [pInnerDia/2+pWall-rdRad,bHeight-rdRad],
    [bInnerDia/2+rdRad,bHeight-rdRad]
    ]);
 
    circle(rdRad);
    }
}
}
