// -> Tiny distance used as clearence between parts:
clear=0.02;

/* [Leg protector] */

// -> The wall thickness of the lower part of the leg protector:
pWidthLow=6;
// -> The wall thickness of the upper part of the leg protector:
pWidthHig=4;
// -> The total height of the leg protector:
pTotalHei=25;
// -> The bottom thickness of the leg protector under leg itself:
pBottomThick=4;


/* [Table leg] */

// -> The width of the table leg:
lWidth=80;
// -> The depth of the table leg:
lDepth=60;


/* [Hidden] */

///////////////////////////////////////////////
// main
difference()
{
    hull()
    {
    rotate([0,0,180])
    makeCornerPiece();

    translate([lWidth, 0])
    rotate([0,0,270])
    makeCornerPiece();

    translate([lWidth, lDepth])
    makeCornerPiece();

    translate([0, lDepth])
    rotate([0,0,90])
    makeCornerPiece();
    }
    
    #translate([0,0,pBottomThick])
    cube([lWidth+clear, lDepth+clear, 3*pTotalHei]);
}
///////////////////////////////////////////////
//
module makeCornerPiece()
{
    rotate_extrude(angle=90, $fn=200)
    polygon(points=[
    [0,0],
    [pWidthLow,0],
    [pWidthHig,pTotalHei],
    [0,pTotalHei]
    ]);
}           
    