// -> Tiny distance used as clearence between parts:
clear=0.02;

/* [Rim] */

// -> The height of the rim:
rHeight=2;
// -> The overhang of the rim:
rOverhang=2;

/* [Body] */

// -> The total height of the adapter:
bHeight=42;
// -> The upper outer diameter of the adapter (cone shaped!):
bUpperOuterDia=50.2;
// -> The lower outer diameter of the adapter (cone shaped!):
bLowerOuterDia=50.0;

// -> The upper inner diameter of the adapter (cone shaped!):
bUpperInnerDia=36.0;
// -> The lower inner diameter of the adapter (cone shaped!):
bLowerInnerDia=34.3;

/* [Hidden] */

rotate_extrude($fn=200)
    polygon(points=[
    [bLowerInnerDia/2,0],
    [bLowerOuterDia/2,0],
    [bUpperOuterDia/2,bHeight-rHeight],
    [bUpperOuterDia/2+rOverhang/2,bHeight-rHeight],
    [bUpperOuterDia/2+rOverhang/2,bHeight],
    [bUpperInnerDia/2,bHeight]
    ]);
    
