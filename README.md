# Contents

This repository keeps all important files that where used during the build of a LowRider CNC machine.

For futher details have a look at the [documentation](https://www.v1engineering.com/lowrider-cnc/) on the [v1engineering homepage](https://www.v1engineering.com). 

[TOC]

---
## SKR v1.4 board with TCM2209 Drivers

---
### Documentation
* [Pdf version of 3dwork's online article about the board configuration](SKR_1.4_Board_with_TCM2209_Drivers/docs/SKR_v1.4 Turbo with TMC2209 Sensorless and TFT35 V3.0 drivers.pdf)
* [SKR v1.4 (Turbo) manual](SKR_1.4_Board_with_TCM2209_Drivers/docs/BTT SKR V1.4 Instruction Manual.pdf)
### Configuration
Configuration files for Marlin 2.0.x and *sensorless* homing:

* [platformio.ini](SKR_1.4_Board_with_TCM2209_Drivers/configuration/Marlin-2.0.x/platformio.ini)
* [Configuration.h](SKR_1.4_Board_with_TCM2209_Drivers/configuration/Marlin-2.0.x/Configuration.h)
* [Configuration_adv.h](SKR_1.4_Board_with_TCM2209_Drivers/configuration/Marlin-2.0.x/Configuration_adv.h)
### Links
* [Complete guide SKR v1.4 / v1.4 Turbo with TMC2209 Sensorless drivers and Display TFT35 V3.0](https://3dwork.io/en/complete-guide-skr-v1-4-and-tmc2209)
* [Article about the TCM drivers](https://3dwork.io/en/tmc-drivers/)
* [BigTreeTech SKR v1.4 Mainboard - 2209 Drivers - Full Install Part 1](https://www.youtube.com/watch?v=oHMZ0ocTYvM)
* [BigTreeTech SKR v1.4 Mainboard - 2209 Drivers - Full Install Part 2](https://www.youtube.com/watch?v=j2FL_mY_LsY)
* [BigTreeTech's git repository](https://github.com/bigtreetech/BIGTREETECH-SKR-V1.3/tree/master/BTT%20SKR%20V1.4)

---
## 800 Watt water cooled spindle with frequency control

---
I decided to buy a water cooled spindle because the cnc machine will be used in a house where other people live.
Therefore I wanted to be the whole process as *silent* as possible.

I bought [this spindle kit](https://de.aliexpress.com/item/4000771386442.html) on aliexpress. The kit consists of:

  * the **spindle** itself:
      *  Brand: G-Penny
      *  Model: BST0.8-65-24K
      *  Size: &#8960;65x195 mm
      *  Collet ER11
      *  220 Volt
      *  0.8 KW
      *  4 Ampere
      *  400 HZ
      *  24000 RPM
      *  Bearings: 2×7002C P4 DT & 2×7000C P4 DT
  * the **inverter**:
      * Brand: G-Penny 
      * Manufacturer: Jiangshu Deoder Electric Co. LTD
      * Model: H100-1.5S
      * Input Voltage: 220V(+/-15%)
      * Input Phase: 1
      * Input Frequency: 48-63 Hz
      * Output Voltage: 0-220v
      * Output Phase: 3 phase  
      * Output Current: 7A
      * Output Frequency: 0-1000Hz
      * Control Method: 1. V/F control ;2. Open-loop flux vector control
      * Inverter Type: QL 220v 1.5kw 1000Hz
  * the **water pump**:
      * Max Head: 3.2 meter
      * Power: 75 watt
      * Maximum flow: 3200 liter/hour
      * Plug Cord Length: 1.4 meter
      * Input voltage: 220V
      * Frequency: 50 HZ
      * Outlet size: 4 and 6 mm
      * Dimensions: 145 * 110 * 90 mm
   *  a **mount bracket 65 mm**
   *  and **5 meter tube** for the water pump.

### Documentation
* [H100-1.5S inverter manual](Watercooled_Spindle_800W/docs/H100_Inverter_Manual.pdf)
* [connector pin assignment](Watercooled_Spindle_800W/docs/water_cooler_connection.jpg)
### Configuration
* [video shows setup of the inverter](Watercooled_Spindle_800W/docs/inverter_setup.mp4)
* [quick document for inverter setup](Watercooled_Spindle_800W/docs/inverter_setup.doc)
### Links
* [800 watt water cooled spindle kit on aliexpress](https://de.aliexpress.com/item/4000771386442.html)

---
## Flat Parts

---

This part is still work in progress.

* [3d printable X template for manual routing](https://www.thingiverse.com/thing:4441659)
* [3d printable Y template for manual routing](https://www.thingiverse.com/thing:3364669)
* [Printable Y_Plate 25mm (stl)](Flat_Parts/Y_plate_25mm.stl)
* [Scad file for printable Y_Plate 25mm](Flat_Parts/Y_plate_25mm.scad)
* [Printable 611_Plate(stl)](Flat_Parts/611_plate_2.stl)
* [Scad file for printable 611_Plate](Flat_Parts/611_plate_2.scad)

---
## Dust Collector

---
This part is still work in progress.
### Documentation
* [Manual for the Dust Commander Cyclone](Dust_Collector/manual_dust-commander_DE.pdf)

---
## Shopping List and Parts

---
This part is probably only interesting for people living in the german speaking EU.

* [Shopping list](Shopping_list_and_parts/ShoppingList.ods)
* [Parts list](Shopping_list_and_parts/PartList.ods)
* [Screws](Shopping_list_and_parts/Schraubenking_Bestellung.pdf)


---
## TODOS

---
* Cutters + Collets
* Power Supply?
* Tube Sizing
* Table Build
* Buildalong?
---
## Author

---
* kaulquappe ([https://bitbucket.org/kaulquappe/](https://bitbucket.org/kaulquappe/))

